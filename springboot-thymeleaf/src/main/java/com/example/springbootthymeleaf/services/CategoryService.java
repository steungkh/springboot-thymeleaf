package com.example.springbootthymeleaf.services;

import com.example.springbootthymeleaf.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAllByStatusActive();
    void create(Category req);
    Category getById(Integer id);
    void delete(Integer id);
}
