package com.example.springbootthymeleaf.services;

import com.example.springbootthymeleaf.models.Category;
import com.example.springbootthymeleaf.repo.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAllByStatusActive() {
        return categoryRepository.findAllByStatus("ACT");
    }

    @Override
    public void create(Category req) {
        req.setStatus("ACT");
        categoryRepository.save(req);
    }

    @Override
    public Category getById(Integer id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Integer id) {
        Category category = categoryRepository.findById(id).orElse(null);
        category.setStatus("DEL");
        categoryRepository.save(category);
    }
}
