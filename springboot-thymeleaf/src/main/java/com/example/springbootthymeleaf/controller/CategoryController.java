package com.example.springbootthymeleaf.controller;

import com.example.springbootthymeleaf.models.Category;
import com.example.springbootthymeleaf.services.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/category")
public class CategoryController {
    private  final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/list")
//    @ResponseBody
    public String index(Model model){
        System.out.println(categoryService.findAllByStatusActive());
        model.addAttribute("categories",categoryService.findAllByStatusActive());
        return "/admin/category/index";
    }

    @GetMapping("/create")
    public String create(Model model){
        model.addAttribute("category",new Category());
        return "/admin/category/create";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute Category category){
        categoryService.create(category);
        return "redirect:/admin/category/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model){
        model.addAttribute("category",categoryService.getById(id));
        return "/admin/category/create";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        categoryService.getById(id);
        categoryService.delete(id);
        return "redirect:/admin/category/list";
    }
}
